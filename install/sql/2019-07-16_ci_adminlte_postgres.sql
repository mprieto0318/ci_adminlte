--
-- Generated from mysql2pgsql.perl
-- http://gborg.postgresql.org/project/mysql2psql/
-- (c) 2001 - 2007 Jose M. Duarte, Joseph Speigle
--

-- warnings are printed for drop tables if they do not exist
-- please see http://archives.postgresql.org/pgsql-novice/2004-10/msg00158.php

-- ##############################################################

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


DROP DATABASE IF EXISTS ci_adminlte;
CREATE DATABASE IF NOT EXISTS ci_adminlte /*!40100 DEFAULT CHARACTER SET latin1 */;
\c ci_adminlte
-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.38-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------
-- Volcando estructura de base de datos para ci_adminlte
-- Volcando estructura para tabla ci_adminlte.admin_preferences
DROP SEQUENCE "if_id_seq" CASCADE ;

CREATE SEQUENCE "if_id_seq"  START WITH 2 ;

CREATE TABLE  "if" NOT EXISTS admin_preferences (
   "id" integer DEFAULT nextval('"if_id_seq"') NOT NULL,
   "user_panel"    smallint NOT NULL DEFAULT '0', 
   "sidebar_form"    smallint NOT NULL DEFAULT '0', 
   "messages_menu"    smallint NOT NULL DEFAULT '0', 
   "notifications_menu"    smallint NOT NULL DEFAULT '0', 
   "tasks_menu"    smallint NOT NULL DEFAULT '0', 
   "user_menu"    smallint NOT NULL DEFAULT '1', 
   "ctrl_sidebar"    smallint NOT NULL DEFAULT '0', 
   "transition_page"    smallint NOT NULL DEFAULT '0', 
   primary key ("id")
)   ;

/*!40000 ALTER TABLE admin_preferences DISABLE KEYS */;
-- Volcando datos para la tabla ci_adminlte.admin_preferences: ~0 rows (aproximadamente)
INSERT INTO "admin_preferences` (`id`", "`user_panel`", "`sidebar_form`", "`messages_menu`", "`notifications_menu`", "`tasks_menu`", "`user_menu`", "`ctrl_sidebar`", "`transition_page`)" VALUES  
	(1, 0, 0, 0, 0, 0, 1, 0, 0);
/*!40000 ALTER TABLE admin_preferences ENABLE KEYS */;

-- Volcando estructura para tabla ci_adminlte.ci_sessions
CREATE TABLE  "if" NOT EXISTS ci_sessions (
   "id"   varchar(128) NOT NULL, 
   "ip_address"   varchar(45) NOT NULL, 
   "timestamp" int CHECK ("timestamp" >= 0) NOT NULL DEFAULT '0',
   "data"   bytea NOT NULL, 
   primary key ("id")
)  ;

/*!40000 ALTER TABLE ci_sessions DISABLE KEYS */;
-- Volcando datos para la tabla ci_adminlte.ci_sessions: ~2 rows (aproximadamente)
INSERT INTO "ci_sessions` (`id`", "`ip_address`", "`timestamp`", "`data`)" VALUES  
	('6fnn5vm66r6q2oqh7kathq4ph5jppqh6', '127.0.0.1', 1563203084, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313536333230333037333B6964656E746974797C733A31353A2261646D696E4061646D696E2E636F6D223B656D61696C7C733A31353A2261646D696E4061646D696E2E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353633323031353331223B6C6173745F636865636B7C693A313536333230333038323B),
	('9nc1fsohuj1sjnk3ns8cn7ls3tsbn108', '127.0.0.1', 1563302228, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313536333330323031343B6964656E746974797C733A31353A2261646D696E4061646D696E2E636F6D223B656D61696C7C733A31353A2261646D696E4061646D696E2E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353633323839333037223B6C6173745F636865636B7C693A313536333239383634343B6D6573736167657C733A33353A223C703E4163636F756E74205375636365737366756C6C7920437265617465643C2F703E223B5F5F63695F766172737C613A313A7B733A373A226D657373616765223B733A333A226F6C64223B7D),
	('cabt85fbk36di80d7hk90kue692jllva', '127.0.0.1', 1563289315, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313536333238393239383B6964656E746974797C733A31353A2261646D696E4061646D696E2E636F6D223B656D61696C7C733A31353A2261646D696E4061646D696E2E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353633323033303832223B6C6173745F636865636B7C693A313536333238393330373B),
	('hc40co2b9gn1j54s4nei314bgc2nnl34', '127.0.0.1', 1563300835, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313536333330303831373B6964656E746974797C733A31353A2261646D696E4061646D696E2E636F6D223B656D61696C7C733A31353A2261646D696E4061646D696E2E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353633323839333037223B6C6173745F636865636B7C693A313536333239383634343B637372666B65797C733A383A2266736F7852634F70223B5F5F63695F766172737C613A323A7B733A373A22637372666B6579223B733A333A226E6577223B733A393A226373726676616C7565223B733A333A226E6577223B7D6373726676616C75657C733A32303A2231756D654B3551443674594F736C4C4639506869223B),
	('noea6592m5g6krlj6m3msilkit69cf69', '127.0.0.1', 1563298696, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313536333239383633313B6964656E746974797C733A31353A2261646D696E4061646D696E2E636F6D223B656D61696C7C733A31353A2261646D696E4061646D696E2E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353633323839333037223B6C6173745F636865636B7C693A313536333239383634343B),
	('o24u8u6jenlqbajvk9r6houvig70g9u2', '127.0.0.1', 1563300268, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313536333330303135323B6964656E746974797C733A31353A2261646D696E4061646D696E2E636F6D223B656D61696C7C733A31353A2261646D696E4061646D696E2E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353633323839333037223B6C6173745F636865636B7C693A313536333239383634343B),
	('sk6ktrhagki5sp6v3ll0ar9tv16m9qgu', '127.0.0.1', 1563300683, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313536333330303437323B6964656E746974797C733A31353A2261646D696E4061646D696E2E636F6D223B656D61696C7C733A31353A2261646D696E4061646D696E2E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353633323839333037223B6C6173745F636865636B7C693A313536333239383634343B);
/*!40000 ALTER TABLE ci_sessions ENABLE KEYS */;

-- Volcando estructura para tabla ci_adminlte.function
DROP SEQUENCE "if_id_seq" CASCADE ;

CREATE SEQUENCE "if_id_seq"  START WITH 21 ;

CREATE TABLE  "if" NOT EXISTS function (
   "id" integer DEFAULT nextval('"if_id_seq"') NOT NULL,
   "name"   varchar(45) NOT NULL, 
   "permission"   varchar(20) DEFAULT NULL, 
   "module_id"   int NOT NULL, 
   primary key ("id")
)   ;

/*!40000 ALTER TABLE function DISABLE KEYS */;
-- Volcando datos para la tabla ci_adminlte.function: ~0 rows (aproximadamente)
INSERT INTO "function` (`id`", "`name`", "`permission`", "`module_id`)" VALUES  
	(1, 'index', '1,2,3,4,5', 1),
	(2, 'index', '1', 2),
	(3, 'index', '1,2', 3),
	(4, 'create', '1,2', 3),
	(5, 'delete', '1,2', 3),
	(6, 'edit', '1,2', 3),
	(7, 'index', '1', 4),
	(8, 'interfaces', '1', 4),
	(9, 'reset_interfaces_admin', '1', 4),
	(10, 'reset_interfaces_public', '1', 4),
	(11, 'index', '1', 5),
	(12, 'index', '1,2', 6),
	(13, 'create', '1,2', 6),
	(14, 'delete', '1,2', 6),
	(15, 'edit', '1,2', 6),
	(16, 'activate', '1,2', 6),
	(17, 'deactivate', '1,2', 6),
	(18, 'profile', '1,2,3,4', 6),
	(19, 'upload_files_ckeditor', '1,2,3,4,5', 7),
	(20, 'index', '1,2,3,4,5', 8);
/*!40000 ALTER TABLE function ENABLE KEYS */;

CREATE INDEX "if_module_id_idx" ON "if" USING btree ("module_id");
ALTER TABLE "if" ADD FOREIGN KEY ("module_id") REFERENCES "module" ("id");
-- Volcando estructura para tabla ci_adminlte.groups
DROP SEQUENCE "if_id_seq" CASCADE ;

CREATE SEQUENCE "if_id_seq"  START WITH 6 ;

CREATE TABLE  "if" NOT EXISTS groups (
   "id" integer DEFAULT nextval('"if_id_seq"') NOT NULL,
   "name"   varchar(20) NOT NULL, 
   "description"   varchar(100) NOT NULL, 
   "bgcolor"   char(7) NOT NULL DEFAULT '#607D8B', 
   primary key ("id")
)   ;

/*!40000 ALTER TABLE groups DISABLE KEYS */;
-- Volcando datos para la tabla ci_adminlte.groups: ~2 rows (aproximadamente)
INSERT INTO "groups` (`id`", "`name`", "`description`", "`bgcolor`)" VALUES  
	(1, 'admin', 'Super Administrator', '#F44336'),
	(2, 'admin_basic', 'Administrator', '#2196F3'),
	(3, 'operator', 'Operator', '#607D8B'),
	(4, 'driver', 'Driver', '#7FB3D5'),
	(5, 'guest', 'Guest', '#82E0AA');
/*!40000 ALTER TABLE groups ENABLE KEYS */;

-- Volcando estructura para tabla ci_adminlte.login_attempts
DROP SEQUENCE "if_id_seq" CASCADE ;

CREATE SEQUENCE "if_id_seq" ;

CREATE TABLE  "if" NOT EXISTS login_attempts (
   "id" integer DEFAULT nextval('"if_id_seq"') NOT NULL,
   "ip_address"   varchar(15) NOT NULL, 
   "login"   varchar(100) NOT NULL, 
   "time" int CHECK ("time" >= 0) DEFAULT NULL,
   primary key ("id")
)  ;

/*!40000 ALTER TABLE login_attempts DISABLE KEYS */;
/*!40000 ALTER TABLE login_attempts ENABLE KEYS */;

-- Volcando datos para la tabla ci_adminlte.login_attempts: ~0 rows (aproximadamente)
-- Volcando estructura para tabla ci_adminlte.module
DROP SEQUENCE "if_id_seq" CASCADE ;

CREATE SEQUENCE "if_id_seq"  START WITH 9 ;

CREATE TABLE  "if" NOT EXISTS module (
   "id" integer DEFAULT nextval('"if_id_seq"') NOT NULL,
   "name"   varchar(45) NOT NULL, 
   primary key ("id")
)   ;

/*!40000 ALTER TABLE module DISABLE KEYS */;
-- Volcando datos para la tabla ci_adminlte.module: ~8 rows (aproximadamente)
INSERT INTO "module` (`id`", "`name`)" VALUES  
	(1, 'Dashboard'),
	(2, 'Database'),
	(3, 'Groups'),
	(4, 'Prefs'),
	(5, 'Resources'),
	(6, 'Users'),
	(7, 'Utilities'),
	(8, 'Home');
/*!40000 ALTER TABLE module ENABLE KEYS */;

-- Volcando estructura para tabla ci_adminlte.public_preferences
DROP SEQUENCE "if_id_seq" CASCADE ;

CREATE SEQUENCE "if_id_seq"  START WITH 2 ;

CREATE TABLE  "if" NOT EXISTS public_preferences (
   "id" integer DEFAULT nextval('"if_id_seq"') NOT NULL,
   "transition_page"    smallint NOT NULL DEFAULT '0', 
   primary key ("id")
)   ;

/*!40000 ALTER TABLE public_preferences DISABLE KEYS */;
-- Volcando datos para la tabla ci_adminlte.public_preferences: ~0 rows (aproximadamente)
INSERT INTO "public_preferences` (`id`", "`transition_page`)" VALUES  
	(1, 0);
/*!40000 ALTER TABLE public_preferences ENABLE KEYS */;

-- Volcando estructura para tabla ci_adminlte.users
DROP SEQUENCE "if_id_seq" CASCADE ;

CREATE SEQUENCE "if_id_seq"  START WITH 5 ;

CREATE TABLE  "if" NOT EXISTS users (
   "id" integer DEFAULT nextval('"if_id_seq"') NOT NULL,
   "ip_address"   varchar(15) NOT NULL, 
   "username"   varchar(100) NOT NULL, 
   "password"   varchar(255) NOT NULL, 
   "salt"   varchar(255) DEFAULT NULL, 
   "email"   varchar(100) NOT NULL, 
   "activation_code"   varchar(40) DEFAULT NULL, 
   "forgotten_password_code"   varchar(40) DEFAULT NULL, 
   "forgotten_password_time" int CHECK ("forgotten_password_time" >= 0) DEFAULT NULL,
   "remember_code"   varchar(40) DEFAULT NULL, 
   "created_on" int CHECK ("created_on" >= 0) NOT NULL,
   "last_login" int CHECK ("last_login" >= 0) DEFAULT NULL,
   "active"  smallint CHECK ("active" >= 0) DEFAULT NULL,
   "first_name"   varchar(50) DEFAULT NULL, 
   "last_name"   varchar(50) DEFAULT NULL, 
   "company"   varchar(100) DEFAULT NULL, 
   "phone"   varchar(20) DEFAULT NULL, 
   primary key ("id")
)   ;

/*!40000 ALTER TABLE users DISABLE KEYS */;
-- Volcando datos para la tabla ci_adminlte.users: ~0 rows (aproximadamente)
INSERT INTO "users` (`id`", "`ip_address`", "`username`", "`password`", "`salt`", "`email`", "`activation_code`", "`forgotten_password_code`", "`forgotten_password_time`", "`remember_code`", "`created_on`", "`last_login`", "`active`", "`first_name`", "`last_name`", "`company`", "`phone`)" VALUES  
	(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', 'NULL', NULL, 'NULL', 1268889823, 1563298644, 1, 'Admin', 'istrator', 'ADMIN', '0'),
	(2, '127.0.0.1', 'admin_basic admin_basic', '$2y$08$B.JY.TJEY85A7f2mPSLGY.N6SKZJf0AGXF6z39thI5hrpgaJncLrS', NULL, 'admin_basic@asd.com', NULL, NULL, NULL, NULL, 1563302058, NULL, 1, 'admin_basic', 'admin_basic', 'admin_basic', '1234567890'),
	(3, '127.0.0.1', 'operator operator', '$2y$08$.xGiocG9zeO8u/K9NGs.wu9FO3g88z016Mt4TFwk/tW8DtHh2Bs8a', NULL, 'operator@asd.com', NULL, NULL, NULL, NULL, 1563302174, NULL, 1, 'operator', 'operator', 'operator', '1234567890'),
	(4, '127.0.0.1', 'driver driver', '$2y$08$tc8BxTtXecuIrn0/eQ1j/Oq31YVXSiCWEKx3yhElbzW0xo4sIR9be', NULL, 'driver@asd.com', NULL, NULL, NULL, NULL, 1563302228, NULL, 1, 'driver', 'driver', 'driver', '1234567890');
/*!40000 ALTER TABLE users ENABLE KEYS */;

-- Volcando estructura para tabla ci_adminlte.users_groups
DROP SEQUENCE "if_id_seq" CASCADE ;

CREATE SEQUENCE "if_id_seq"  START WITH 11 ;

CREATE TABLE  "if" NOT EXISTS users_groups (
   "id" integer DEFAULT nextval('"if_id_seq"') NOT NULL,
   "user_id" int CHECK ("user_id" >= 0) NOT NULL,
   "group_id"  integer CHECK ("group_id" >= 0) NOT NULL,
   primary key ("id"),
 unique ("user_id", "group_id") 
)   ;

/*!40000 ALTER TABLE users_groups DISABLE KEYS */;
-- Volcando datos para la tabla ci_adminlte.users_groups: ~0 rows (aproximadamente)
INSERT INTO "users_groups` (`id`", "`user_id`", "`group_id`)" VALUES  
	(1, 1, 1),
	(7, 2, 2),
	(9, 3, 3),
	(10, 4, 4);
/*!40000 ALTER TABLE users_groups ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
CREATE INDEX "if_user_id_idx" ON "if" USING btree ("user_id");
CREATE INDEX "if_group_id_idx" ON "if" USING btree ("group_id");
ALTER TABLE "if" ADD FOREIGN KEY ("group_id") REFERENCES "groups" ("id");
ALTER TABLE "if" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");
