<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title><?= $pagetitle; ?></title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>

    <link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/landing-page/theme/fonts/icomoon/style.css'); ?>">

  <link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/landing-page/theme/css/bootstrap.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/landing-page/theme/css/jquery-ui.css'); ?>">
</head>
<body>
    <center>
        <h4><?= $pagetitle; ?></h4>
        <!-- Mensajes -->
        <div class="col-md-12">
            <?= render_messages(); ?>
        </div><!-- /Mensajes -->
    </center>

    <script src="<?php echo base_url($frameworks_dir . '/landing-page/theme/js/jquery-3.3.1.min.js'); ?>"></script>
  <script src="<?php echo base_url($frameworks_dir . '/landing-page/theme/js/jquery-migrate-3.0.1.min.js'); ?>"></script>
  <script src="<?php echo base_url($frameworks_dir . '/landing-page/theme/js/jquery-ui.js'); ?>"></script>
  <script src="<?php echo base_url($frameworks_dir . '/landing-page/theme/js/popper.min.js'); ?>"></script>
  <script src="<?php echo base_url($frameworks_dir . '/landing-page/theme/js/bootstrap.min.js'); ?>"></script>
</body>
</html>
