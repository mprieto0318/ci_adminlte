<?php
defined('BASEPATH') or exit('No direct script access allowed');
// echo '<pre>';
// print_r($groups);
// print_r($acl);
// exit;
?>


<div class="content-wrapper">
  <section class="content-header">
    <!-- Mensajes -->
    <div class="row">
      <div class="col-md-12">
        <?= render_messages(); ?>
      </div>
    </div><!-- /Mensajes -->
    
    <?php echo $pagetitle; ?>
    <?php echo $breadcrumb; ?>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-md-12">

        <div class="box">
          <?php echo form_open(current_url(), array('class' => 'form-horizontal', 'id' => 'form-edit_permission')); ?>
          <div class="box-header with-border">
            <h3 class="box-title">Otorgar o remover accesos a funcionalidades del sistema</h3>
            <div class="box-tools pull-right">
              <!-- Buttons, labels, and many other things can be placed here! -->
              <!-- Here is a label for example -->
              <span class="label label-primary">Label</span>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">

            <table class="table table-hover">
              <thead>
                <tr>
                  <!-- First column header is not rotated -->
                  <th scope="col" class="text-center">MODULO</th>
                  <th scope="col" class="text-center">FUNCIÓN</th>
                  <!-- Following headers are rotated -->
                  <?php foreach ($groups as $key => $value) : ?>
                    <th class="text-center" style="background: <?= $value['bgcolor']; ?>;">
                      <div><small><?= strtoupper($value['name']); ?></small></div>
                    </th>
                  <?php endforeach; ?>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($acl as $key_m => $value_m) : ?>
                  <?php $count_f = 0; ?>
                  <?php foreach ($value_m as $key_f => $value_f) : ?>

                    <!-- Titulo del modulo -->
                    <?php if ($value_f == 'INI') : ?>
                      <tr>
                        <th class="text-center bg-info" scope="row" rowspan="<?= count($value_m); ?>"><small><?= strtoupper($key_m); ?></small></th>
                      </tr>
                      <?php
                      continue;
                    endif;
                    ?>
                    <?php $count_f++; ?>
                    <!-- /Titulo del modulo -->

                    <tr>
                      <th><small><?= strtoupper($key_f); ?></small></th>

                      <?php foreach ($groups as $key_g => $value_g) : ?>
                        <?php
                        $pos = strpos($value_f, $value_g['id']);
                        // Capturar checkeado
                        $cb_checked = ($pos !== false) ? 'checked' : '';
                        // Desactivar checkbox ADMIN
                        $disabled = ($value_g['id'] == 1) ? 'disabled' : '';
                        ?>

                        <td>
                          <div class="form-check text-center">
                            <input class="form-check-input" type="checkbox" value="<?= $value_g['id']; ?>" id="defaultCheck1" name="<?= $key_m; ?>[<?= $key_f; ?>][]" <?= $cb_checked; ?> <?= $disabled; ?>>
                          </div>
                        </td>
                      <?php endforeach; ?>

                    </tr>


                    <?php if (count($value_m) - 1 == $count_f) : ?>
                      <tr>
                        <th colspan="<?= count($groups) + 2; ?>" style="background:#dedede;"></th>
                      </tr>
                    <?php endif; ?>

                  <?php endforeach; ?>


                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <div class="btn-group pull-right">
                  <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => lang('actions_reset'))); ?>
                  <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?>
                </div>
              </div>
            </div>
          </div>
          <!-- box-footer -->
          <?php echo form_close(); ?>
        </div><!-- /.box -->

      </div>
    </div>
  </section>
</div>