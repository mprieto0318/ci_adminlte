<?php
defined('BASEPATH') or exit('No direct script access allowed');
// echo '<pre>';
// print_r($groups);
// print_r($acl);
// exit;
?>


<div class="content-wrapper">
  <section class="content-header">
    <?php echo $pagetitle; ?>
    <?php echo $breadcrumb; ?>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-md-12">

        <div class="box">
          <?php echo form_open(current_url(), array('class' => 'form-horizontal', 'id' => 'form-edit_permission')); ?>
          <div class="box-header with-border">
            <h3 class="box-title">Configuración Menu de Administración</h3>
            <div class="box-tools pull-right">
              <!-- Buttons, labels, and many other things can be placed here! -->
              <!-- Here is a label for example -->
              <span class="label label-primary">Label</span>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            ----- DB ----

            CREATE TABLE `menu` (
            `id` INT(11) NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(30) NOT NULL,
            `weight` INT(11) NULL DEFAULT NULL,
            `status` INT(11) NULL DEFAULT NULL,
            `parent_id` INT(11) NULL DEFAULT '0' COMMENT '0 if menu is root level or menuid if this is child on any menu',
            `function_id` INT(11) NOT NULL,
            PRIMARY KEY (`id`),
            INDEX `fk_menu_function1` (`function_id`),
            CONSTRAINT `fk_menu_function1` FOREIGN KEY (`function_id`) REFERENCES `function` (`id`)
            )
            COMMENT='Menu administrador'
            COLLATE='latin1_swedish_ci'
            ENGINE=InnoDB
            ;



            ----- /DB -----




          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <div class="btn-group pull-right">
                  <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => lang('actions_reset'))); ?>
                  <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?>
                </div>
              </div>
            </div>
          </div>
          <!-- box-footer -->
          <?php echo form_close(); ?>
        </div><!-- /.box -->

      </div>
    </div>
  </section>
</div>

