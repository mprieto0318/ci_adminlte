<?php
defined('BASEPATH') or exit('No direct script access allowed');

?>

<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b><?php echo lang('footer_version'); ?></b> Development
  </div>
  <strong><?php echo lang('footer_copyright'); ?> &copy; 2014-<?php echo date('Y'); ?> <a href="http://almsaeedstudio.com" target="_blank">Almsaeed Studio</a> &amp; <a href="https://domprojects.com" target="_blank">domProjects</a>.</strong> <?php echo lang('footer_all_rights_reserved'); ?>.
</footer>
</div>

<script src="<?php echo base_url($frameworks_dir . '/jquery/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/bootstrap/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url($plugins_dir . '/slimscroll/slimscroll.min.js'); ?>"></script>
<?php if ($mobile == TRUE) : ?>
  <script src="<?php echo base_url($plugins_dir . '/fastclick/fastclick.min.js'); ?>"></script>
<?php endif; ?>
<?php if ($admin_prefs['transition_page'] == TRUE) : ?>
  <script src="<?php echo base_url($plugins_dir . '/animsition/animsition.min.js'); ?>"></script>
<?php endif; ?>
<?php if ($this->router->fetch_class() == 'users' && ($this->router->fetch_method() == 'create' or $this->router->fetch_method() == 'edit')) : ?>
  <script src="<?php echo base_url($plugins_dir . '/pwstrength/pwstrength.min.js'); ?>"></script>
<?php endif; ?>
<?php if ($this->router->fetch_class() == 'groups' && ($this->router->fetch_method() == 'create' or $this->router->fetch_method() == 'edit')) : ?>
  <script src="<?php echo base_url($plugins_dir . '/tinycolor/tinycolor.min.js'); ?>"></script>
  <script src="<?php echo base_url($plugins_dir . '/colorpickersliders/colorpickersliders.min.js'); ?>"></script>
<?php endif; ?>
<script src="<?php echo base_url($frameworks_dir . '/adminlte/js/adminlte.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/domprojects/js/dp.min.js'); ?>"></script>

<!-- JS LIBRARIES -->
<script src="<?php echo base_url($plugins_dir . '/ckeditor/ckeditor.js'); ?>"></script> <!-- Configuración General! /ckeditor/config.js -->
<script src="<?php echo base_url($plugins_dir . '/bootstrap-select/bootstrap-select.min.js'); ?>"></script>
<script src="<?php echo base_url($plugins_dir . '/bootstrap-select/i18n/defaults-es_ES.js'); ?>"></script>
<script src="<?php echo base_url($plugins_dir . '/datatables/datatables.min.js'); ?>"></script>


<!-- JS CUSTOM -->

</body>

</html>