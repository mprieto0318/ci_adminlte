<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends Admin_Controller
{

  public function __construct()
  {
    parent::__construct();

    /* Load :: Common */
    $this->load->library('cart');        
    $this->load->model('product');

    /* Breadcrumbs :: Common */
    $this->breadcrumbs->unshift(1, lang('menu_products'), 'admin/prefs');
  }


  public function index() {
    /* Title Page */
    $this->page_title->push(lang('menu_dashboard'));
    $this->data['pagetitle'] = $this->page_title->show();

    /* Breadcrumbs */
    $this->data['breadcrumb'] = $this->breadcrumbs->show();

    /* Data */
    $this->data['count_users']       = $this->dashboard_model->get_count_record('users');
    $this->data['count_groups']      = $this->dashboard_model->get_count_record('groups');
    $this->data['disk_totalspace']   = $this->dashboard_model->disk_totalspace(DIRECTORY_SEPARATOR);
    $this->data['disk_freespace']    = $this->dashboard_model->disk_freespace(DIRECTORY_SEPARATOR);
    $this->data['disk_usespace']     = $this->data['disk_totalspace'] - $this->data['disk_freespace'];
    $this->data['disk_usepercent']   = $this->dashboard_model->disk_usepercent(DIRECTORY_SEPARATOR, FALSE);
    $this->data['memory_usage']      = $this->dashboard_model->memory_usage();
    $this->data['memory_peak_usage'] = $this->dashboard_model->memory_peak_usage(TRUE);
    $this->data['memory_usepercent'] = $this->dashboard_model->memory_usepercent(TRUE, FALSE);


    /* TEST */
    $this->data['url_exist']    = is_url_exist('http://www.domprojects.com');


    /* Load Template */
    $this->template->admin_render('admin/dashboard/index', $this->data);
  }

  function index(){
      $data = array();
      
      // Fetch products from the database
      $data['products'] = $this->product->getRows();
      
      // Load the product list view
      $this->load->view('products/index', $data);
  }

  function addToCart($proID){
      
      // Fetch specific product by ID
      $product = $this->product->getRows($proID);
      
      // Add product to the cart
      $data = array(
          'id'    => $product['id'],
          'qty'    => 1,
          'price'    => $product['price'],
          'name'    => $product['name'],
          'image' => $product['image']
      );
      $this->cart->insert($data);
      
      // Redirect to the cart page
      redirect('cart/');
  }
}
