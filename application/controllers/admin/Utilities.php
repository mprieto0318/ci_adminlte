<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Utilities extends Admin_Controller
{

  public function __construct()
  {
    parent::__construct();

    /* Load :: Common */
    //$this->load->helper('number');
    //$this->load->model('admin/dashboard_model');
  }


  public function index()
  {
    
  }

  function upload_files_ckeditor() {
    if (!$this->ion_auth->logged_in() or !$this->ion_auth->is_admin()) {
      redirect('auth/login', 'refresh');
    } else {
      // Parameters
      $type = $_GET['type'];
      $CKEditor = $_GET['CKEditor'];
      $funcNum = $_GET['CKEditorFuncNum'];

      $message = '';

      // Image upload
      if ($type == 'image') {

        $allowed_extension = array(
          "png", "jpg", "jpeg"
        );

        // Get image file extension
        $file_extension = pathinfo($_FILES["upload"]["name"], PATHINFO_EXTENSION);

        if (in_array(strtolower($file_extension), $allowed_extension)) {

          $path_file = './upload/ckeditor/images/' . $_FILES['upload']['name'];

          if (move_uploaded_file($_FILES['upload']['tmp_name'], $path_file)) {       
            $url = base_url("/upload/ckeditor/images/" . $_FILES['upload']['name']);
            echo '<script>window.parent.CKEDITOR.tools.callFunction(' . $funcNum . ', "' . $url . '", "' . $message . '")</script>';
          }
        }

        exit;
      }

      // File upload
      if ($type == 'file') {

        $allowed_extension = array(
          "doc", "pdf", "docx"
        );

        // Get image file extension
        $file_extension = pathinfo($_FILES["upload"]["name"], PATHINFO_EXTENSION);

        if (in_array(strtolower($file_extension), $allowed_extension)) {
          $path_file = './upload/ckeditor/files/' . $_FILES['upload']['name'];

          if (move_uploaded_file($_FILES['upload']['tmp_name'], $path_file)) {
            $url = base_url("/upload/ckeditor/files/" . $_FILES['upload']['name']);
            echo '<script>window.parent.CKEDITOR.tools.callFunction(' . $funcNum . ', "' . $url . '", "' . $message . '")</script>';
          }
        }

        exit;
      }
    }
  }
}
