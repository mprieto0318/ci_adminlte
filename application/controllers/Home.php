<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Public_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->lang->load('public/home');
    }


	public function index()
	{
		$this->load->view('public/home', $this->data);
    }
    
    public function access_denied() {
        /* Title Page */
        $this->data['pagetitle'] = lang('access_denied');
        $this->load->view('public/access_denied', $this->data);
    }
}
