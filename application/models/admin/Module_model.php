<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Module_model extends CI_Model
{

  public $tbl;

  public function __construct()
  {
    parent::__construct();
    $this->tbl = 'module';
  }

  function insert($values)
  {
    $this->db->insert($this->tbl, $values);
    return $this->db->insert_id();
  }

  function update($id, $values)
  {
    $this->db->where('id', $id);
    $this->db->update($this->tbl, $values);

    return $this->db->affected_rows();
  }

  function get_module($values = null) {
    if ($values) {
      $this->db->where($values);
    }

    $this->db->order_by("name", "asc");

    $q = $this->db->get($this->tbl);

    if ($q->num_rows() > 0) {
      return $q->result();
    } else {
      return FALSE;
    }
  }

  function get_function($values = null)
  {
    if ($values) {
      $this->db->where($values);
    }
    
    $this->db->order_by("name", "asc");
    $q = $this->db->get('function');

    if ($q->num_rows() > 0) {
      return $q->result();
    } else {
      return FALSE;
    }
  }


  function get_acl_config() {
    $module = $this->get_module();
    $function = $this->get_function();

    $result = [];
    foreach ($module as $value_m) {

      // Se agrega para que todos los modulos se agreguen al array $result
      // NOTA: NO SE PUEDE BORRAR, SE UTILIZA EN MAS LUGARES
      $result[strtolower($value_m->name)][] = 'INI'; 

      foreach ($function as $value_f) {
        if ($value_f->module_id == $value_m->id) {
          $result[strtolower($value_m->name)][strtolower($value_f->name)] = $value_f->permission;
          $result[strtolower($value_m->name)][strtolower($value_f->name)] = $value_f->permission;
        }
      }
    }
    
    return $result;
  }

  

  // NOTA: NO SE PUEDE BORRAR/EDITAR, SE UTILIZA EN MAS LUGARES
  function get_menu_config() {
    $module = $this->get_module();
    $function = $this->get_function();

    $result = [];
    foreach ($module as $key_m => $value_m) {

      $result[$key_m]['id'] = $value_m->id;
      $result[$key_m]['name'] = $value_m->name;
      $result[$key_m]['functions'] = [];
      foreach ($function as $key_f => $value_f) {
        if ($value_f->module_id == $value_m->id) {
          $result[$key_m]['functions'][$key_f]['id'] = $value_f->id;
          $result[$key_m]['functions'][$key_f]['name'] = $value_f->name;
          $result[$key_m]['functions'][$key_f]['permission'] = $value_f->permission;
        }
      }
    }

    return $result;
  }
}
