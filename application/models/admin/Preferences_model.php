<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Preferences_model extends CI_Model
{

  public function __construct()
  {
    parent::__construct();
  }


  public function get_interface($table)
  {
    $query = $this->db->get($table);

    if ($query->num_rows() > 0) {
      return $query->result_array();
    } else {
      return FALSE;
    }
  }


  public function update_interfaces($table, $data)
  {
    $where = "id = 1";

    return $this->db->update($table, $data, $where);
  }

  function get_module_function($values = '') {
    $selects = 'm.id id_module, m.name name_module, f.id id_function, f.name name_function, f.permission';

    $q = $this->db->select($selects);
    $q->join('function f', 'f.module_id = m.id');

    $q->order_by("m.name", "asc");
    $q->order_by("f.name", "asc");

    if ($values) {
      $q->where($values);
    }

    return $q->get('module m')->result();
  }

  // SIN USO - PENDIENTE MEJORA
  public function get_menu($values = null){
    $selects = 'me.id id_menu, me.name name_menu, me.weight, me.status status_menu, me.parent_id ,mo.id id_module, mo.name name_module, f.id id_function, f.name name_function, f.permission';

    $q = $this->db->select($selects);
    $q->join('function f', 'f.id = me.function_id');
    $q->join('module mo', 'mo.id = f.module_id');

    $q->order_by("me.id", "asc");
    $q->order_by("me.weight", "asc");
    $q->order_by("mo.name", "asc");
    $q->order_by("f.name", "asc");

    if ($values) {
      $q->where($values);
    }

    return $q->get('menu me')->result();
  }
}
