<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Function_model extends CI_Model
{

  public $tbl;

  public function __construct()
  {
    parent::__construct();
    $this->tbl = 'function';
  }

  function get($values = null) {
    if ($values) {
      $this->db->where($values);
    }

    $q = $this->db->get($this->tbl);

    if ($q->num_rows() > 0) {
      return $q->result();
    } else {
      return FALSE;
    }
  }

  function insert($values)
  {
    $this->db->insert($this->tbl, $values);
    return $this->db->insert_id();
  }

  function update($where, $values) {
    $this->db->where($where);
    $this->db->update($this->tbl, $values);

    return $this->db->affected_rows();
  }



  // ---- SIN USO ------



  


  function get_acl_config() {
    $module = $this->get_module();
    $function = $this->get_function();

    $result = [];
    foreach ($module as $m_value) {
      foreach ($function as $f_value) {
        if ($f_value->module_id == $m_value->id) {
          $result[strtolower($m_value->name)][strtolower($f_value->name)] = $f_value->permission;
         // break;
        }
      }
    }

    return $result;
  }
}
