<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('send_mail')) {
  function send_mail($to, $subj, $tpl, $data) {
    $CI    = &get_instance();
    $CI->load->library('email');
    $CI->config->load('ion_auth', TRUE);

    $email_config = $CI->config->item('email_config', 'ion_auth');
    $CI->email->initialize($email_config);
    $CI->email->set_newline("\r\n");

    $message = $CI->load->view("../views/common/_templates/email/$tpl/index.tpl.php", $data, TRUE);
    $CI->email->clear();
    $CI->email->from($CI->config->item('admin_email', 'ion_auth'), $CI->config->item('site_title', 'ion_auth'));
    $CI->email->to($to);
    $CI->email->subject($subj);
    $CI->email->message($message);

    if ($CI->email->send()) {
      $msg = "<b>COD_S #3</b> - Mensaje enviado a <b>$to</b>";
      save_messages('success', $msg); // COD_S #3
      return TRUE;
    }

    $msg = '<b>COD_E #3</b> - Se requiere soporte' . $CI->email->print_debugger();;
    save_messages('danger', $msg); // COD_E #3
    return FALSE;
    
  }
}

/*
if (!function_exists('get_active')) {
  function get_active($pages, &$active) {
      $CI    = &get_instance();
    
      foreach ($pages as $key => $value) {
          if(isset($value['pages'])) {
            if($active == 'active') {
              $active = 'active';
              return true;
            }
            get_active($value['pages'], $active);
          }else {
            if (isset($value['class'], $value['function']) && $value['class'] == $CI->router->fetch_class() && $value['function'] == $CI->router->fetch_method()) {
              $active = 'active';
              return true;
            }elseif (!isset($value['class'], $value['function']) && $value['route'] != '#') {
              $msg = 'Mensaje del sistema: <b>COD_E #2</b> - Se requiere soporte';
              save_messages('danger', $msg); // COD_E #2
            }
          }
      }

    return false;
  }
}

if (!function_exists('bootstrap_menu')) {
  function bootstrap_menu($menu) {
    $CI = &get_instance();
    $CI->load->model('admin/module_model', 'MODULE_DB');

    $acl = $CI->MODULE_DB->get_acl_config();    
    $html_out  = "";

    foreach ($menu as $key_menu => $value) {
      if (isset($value["class"], $value["function"], $acl[$value["class"]][$value["function"]])) {  
        $permission = explode(',', $acl[$value["class"]][$value["function"]]); // Convertir a array
        
        foreach ($permission as $key => &$value_perm) { 
          $value_perm = (int) $value_perm; // necesario cast() para "$this->ion_auth->in_group()"
        }
        
        // validar grupo de usuario
        if (!$CI->ion_auth->in_group($permission)) {
          // No renderiza item del menu por permisos insuficientes
          continue;
        }



        // -------------------------------------------------
        // -------------------------------------------------
        // --------------- ACCESO CONCEDIDO! ---------------
        // ----- Si se llega a este punto, el usuario  ------
        // ---------- tiene acceso al menu -----------------
        // -------------------------------------------------
        // -------------------------------------------------
      
      }else {
        if(!isset($value['pages'])) {
          $msg_error = 'El item <b>' . $value['title'] . "</b> del menu, no esta registrado en el sistema<br>";
  
          $msg = 'Mensaje del sistema: <b>COD_W #2</b><br>' . $msg_error;
          save_messages('warning', $msg); // COD_W #1
          continue;
        }

      }

      $active = '';
      $class_drowdown = '';
      $icon_drowdown = '';
      $icon = ($value['icon'] != '') ? '<i class="fa ' . $value['icon'] . '"></i>' : '';

      if (isset($value['pages'])) {
        $class_drowdown = 'treeview ';
        $icon_drowdown = '<i class="fa fa-angle-left pull-right"></i>';
        
        // Obtener estado "active" para item <li> del menu
        get_active($value['pages'], $active);
      }else {
        if ($value['class'] == $CI->router->fetch_class() && $value['function'] == $CI->router->fetch_method()) {
          $active = 'active';
        }
      }

      $html_out .= '
        <li class="' . $class_drowdown . $active .'">
          <a href="' . site_url($value['route']) . '">' . $icon . ' 
          <span>' . $value['title'] . '</span>
          ' . $icon_drowdown . '
          </a>';

      if (isset($value['pages'])) {
        $html_out .= '<ul class="treeview-menu">';
        $html_out .= bootstrap_menu($value['pages']);
        $html_out .= "</ul>";
      }
      $html_out .= '</li>';
    }

    return $html_out;
  }
}
*/
