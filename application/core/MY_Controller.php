<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();

    /* COMMON :: ADMIN & PUBLIC */
    /* Load */
    $this->load->database();

    /* Data */
    $this->data['lang'] = element($this->config->item('language'), $this->config->item('language_abbr'));
    $this->data['charset'] = $this->config->item('charset');
    $this->data['frameworks_dir'] = $this->config->item('frameworks_dir');
    $this->data['plugins_dir'] = $this->config->item('plugins_dir');
    $this->data['avatar_dir'] = $this->config->item('avatar_dir');

    /* Any mobile device (phones or tablets) */
    if ($this->mobile_detect->isMobile()) {
      $this->data['mobile'] = TRUE;

      if ($this->mobile_detect->isiOS()) {
        $this->data['ios'] = TRUE;
        $this->data['android'] = FALSE;
      } elseif ($this->mobile_detect->isAndroidOS()) {
        $this->data['ios'] = FALSE;
        $this->data['android'] = TRUE;
      } else {
        $this->data['ios'] = FALSE;
        $this->data['android'] = FALSE;
      }

      if ($this->mobile_detect->getBrowsers('IE')) {
        $this->data['mobile_ie'] = TRUE;
      } else {
        $this->data['mobile_ie'] = FALSE;
      }
    } else {
      $this->data['mobile'] = FALSE;
      $this->data['ios'] = FALSE;
      $this->data['android'] = FALSE;
      $this->data['mobile_ie'] = FALSE;
    }


     // -----------------------------------------------------------
    // ---------------------- CONTROL ACL -----------------------
    // -----------------------------------------------------------

    if ($this->ion_auth->logged_in()) {
      $this->control_acl();
    }

    // -----------------------------------------------------------
    // ---------------------- /CONTROL ACL -----------------------
    // -----------------------------------------------------------
  }

  function control_acl() {
    $this->load->model('admin/module_model', 'MODULE_DB');
    $this->load->model('admin/function_model', 'FUNCTION_DB');

    
    //$module = $this->ci->router->fetch_module(); // no es HMVC
    $class = $this->router->fetch_class();
    $method = $this->router->fetch_method();
    
    $ignore_module = ['access_denied'];
    foreach ($ignore_module as $value) {
      if($method == $value) {
        return false;
      }
    }


  
    $acl = $this->MODULE_DB->get_acl_config();
    // - $this->ion_auth->get_users_groups()->result(); // Obtener grupos del usuario en sesión
  
    if (isset($acl[$class][$method])) {  // validar grupo de usuario
      $permission = explode(',', $acl[$class][$method]); // Convertir a array
  
      foreach ($permission as $key => &$value) {
        $value = (int) $value; // necesario cast() para "$this->ion_auth->in_group()"
      }
  
  
      if (!$this->ion_auth->in_group($permission)) {
        // Permiso Denegado
        
        $msg_error = 'El grupo al que pertenece no tiene acceso a ' . $class . ' ->' . $method . '<br>';
        // echo $msg_error;
        // var_dump($this->ion_auth->in_group($permission));
        // exit;
  
        $msg = 'Mensaje del sistema: <b>COD_W #1</b><br>'. $msg_error;
        save_messages('warning', $msg); // COD_W #1

        redirect('/public/access-denied');
        
        //$this->template->auth_render('public/access_denied');

      }
        
  
  
      // -------------------------------------------------
      // -------------------------------------------------
      // --------------- ACCESO CONCEDIDO! ----------------
      // ----- Si se llega a este punto el usuario  ------
      // ----------------- tiene acceso ------------------
      // -------------------------------------------------
      // -------------------------------------------------
      
  
    }else {  // Controller o function no existen en la db->module - db->function
      // Validar que no exista el registro para crearlo
      $result_module = $this->MODULE_DB->get_module(['name' => ucwords($class)]);
      
      if (!isset($acl[$class])) { // Si NO! existe el modulo; por ende no existe la funcion
  
        if ($result_module == FALSE) {
          // Guardar Modulo
          $result = $this->MODULE_DB->insert(['name' => ucwords($class)]); // $result = id del registro creado
  
          if ($result > 0) {
            $msg = 'Mensaje del sistema: <b>COD_S #1</b>';
            save_messages('success', $msg); // COD_S #1
            
            // Guardar Funcion
            $values = [
              'name' => $method, 
              'permission' => '1',
              'module_id' => $result
            ];
  
            $result = $this->FUNCTION_DB->insert($values); // $result = id del registro creado
  
            $msg = 'Mensaje del sistema: <b>COD_S #2</b>';
            save_messages('success', $msg); // COD_S #2
          }
        }
        // echo "Retorna a la misma url pero ya va a existir el modulo y la funcion en la db";
        // exit;
        // Retorna a la misma url pero ya va a existir el modulo y la funcion en la db
        redirect(current_url());
  
      }elseif (!isset($acl[$class][$method])) { // SI existe el modulo; pero NO! existe la funcion
        if ($result_module != FALSE) {


          
          
          // Guardar Funcion
          $values = [
            'name' => $method,
            'permission' => '1',
            'module_id' => $result_module[0]->id
          ];
          // echo "class: $class <br>";
          // echo '<pre>';
          // print_r($values);
          // exit;

          $result = $this->FUNCTION_DB->insert($values); // $result = id del registro creado
  
          $msg = 'Mensaje del sistema: <b>COD_S #3</b>';
          save_messages('success', $msg); // COD_S #3
  
          // Retorna a la misma url pero ya va a existir la funcion en la db
          redirect(current_url());
          //redirect('/public/access-denied');
        }
      }
      
      // echo "SI SE LLEGA A ESTE PUNTO, A OCURRIDO UN ERROR EN $acl[$class][$method]";
      // exit;
      // SI SE LLEGA A ESTE PUNTO, A OCURRIDO UN ERROR EN $acl[$class][$method]
      // Se debe validar
      $msg = 'Mensaje del sistema: <b>COD_E #1</b> - Se requiere soporte';
      save_messages('danger', $msg); // COD_E #1
      //redirect('/');
      redirect('/public/access-denied');
    }
  }
}



class Admin_Controller extends MY_Controller
{
  public function __construct()
  {
    parent::__construct();

    //if (!$this->ion_auth->logged_in() or !$this->ion_auth->is_admin()) {
    if (!$this->ion_auth->logged_in()) {
      redirect('auth/login', 'refresh');
    } else {
      //echo "entro al admin"; exit;
      /* Load */
      $this->load->config('admin/dp_config');
      $this->load->library(['admin/breadcrumbs', 'admin/page_title']);
      $this->load->model('admin/core_model');
      $this->load->helper('my_menu');
      $this->lang->load(['admin/main_header', 'admin/main_sidebar', 'admin/footer', 'admin/actions']);

      /* Load library function  */
      $this->breadcrumbs->unshift(0, $this->lang->line('menu_dashboard'), 'admin/dashboard');

      /* Data */
      $this->data['title'] = $this->config->item('title');
      $this->data['title_lg'] = $this->config->item('title_lg');
      $this->data['title_mini'] = $this->config->item('title_mini');
      $this->data['admin_prefs'] = $this->prefs_model->admin_prefs();
      $this->data['user_login'] = $this->prefs_model->user_info_login($this->ion_auth->user()->row()->id);

      
      //$routes = $this->router->routes;
      //print_r($routes); exit;

      // $all_lang_array = $this->lang->language;
      // print_r($all_lang_array); exit;


      $this->data['menu_admin'] = $this->config->item('menu_admin');

      // echo '<pre>';
      // print_r($this->data['menu_admin']);
      // exit;


      if ($this->router->fetch_class() == 'dashboard') {
        $this->data['dashboard_alert_file_install'] = $this->core_model->get_file_install();
        $this->data['header_alert_file_install'] = NULL;
      } else {
        $this->data['dashboard_alert_file_install'] = NULL;
        $this->data['header_alert_file_install'] = NULL; /* << A MODIFIER !!! */
      }
    }
  }
}


class Public_Controller extends MY_Controller
{
  public function __construct()
  {
    parent::__construct();

    if ($this->ion_auth->logged_in()) {
    //if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
      $this->data['admin_link'] = TRUE;
    } else {
      $this->data['admin_link'] = FALSE;
    }

    if ($this->ion_auth->logged_in()) {
      $this->data['logout_link'] = TRUE;
    } else {
      $this->data['logout_link'] = FALSE;
    }
  }
}
