<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['language_abbr'] = array(
    'spanish' => 'sp',
    'english'    => 'en',
    'french'     => 'fr',
    'portuguese' => 'pt'
);
