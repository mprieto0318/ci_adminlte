<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['admin'] = 'admin/dashboard';
$route['public/access-denied'] = 'home/access_denied';

// -------------------------------------------------
// ----------------- admin/dashboard --------------------
// -------------------------------------------------
$route['admin/dashboard'] = 'admin/dashboard/index';
# $route['admin/dashboard/XXX'] = 'admin/dashboard/XXX';


// -------------------------------------------------
// ----------------- admin/user --------------------
// -------------------------------------------------
$route['admin/users'] = 'admin/users/index';
$route['admin/users/create'] = 'admin/users/create';
$route['admin/users/delete'] = 'admin/users/delete';
$route['admin/users/edit'] = 'admin/users/edit';
$route['admin/users/deactivate'] = 'admin/users/deactivate';
$route['admin/users/profile'] = 'admin/users/profile';
# $route['admin/users/XXX'] = 'admin/users/XXX';


// -------------------------------------------------
// ----------------- admin/groups ------------------
// -------------------------------------------------
$route['admin/groups'] = 'admin/groups/index';
$route['admin/groups/create'] = 'admin/groups/create';
$route['admin/groups/delete'] = 'admin/groups/delete';
$route['admin/groups/edit'] = 'admin/groups/edit';
# $route['admin/groups/XXX'] = 'admin/groups/XXX';


// -------------------------------------------------
// ----------------- admin/pref --------------------
// -------------------------------------------------
$route['admin/prefs/interfaces/(:any)'] = 'admin/prefs/interfaces/$1';
$route['admin/prefs/permission'] = 'admin/prefs/permission';
# $route['admin/prefs/XXX'] = 'admin/prefs/XXX';


// -------------------------------------------------
// ----------------- admin/database --------------------
// -------------------------------------------------
$route['admin/database'] = 'admin/database/index';
# $route['admin/database/XXX'] = 'admin/database/XXX';



// -------------------------------------------------
// ----------------- admin/files --------------------
// -------------------------------------------------
$route['admin/files'] = 'admin/files/index';
# $route['admin/files/XXX'] = 'admin/files/XXX';


// -------------------------------------------------
// ----------------- admin/resources --------------------
// -------------------------------------------------
$route['admin/resources'] = 'admin/resources/index';
# $route['admin/resources/XXX'] = 'admin/resources/XXX';


// -------------------------------------------------
// ----------------- admin/license --------------------
// -------------------------------------------------
$route['admin/license'] = 'admin/license/index';
# $route['admin/resources/XXX'] = 'admin/resources/XXX';


// -------------------------------------------------
// ----------------- admin/vehiculos --------------------
// -------------------------------------------------
$route['admin/vehiculos'] = 'admin/vehiculos/index';
# $route['admin/resources/XXX'] = 'admin/resources/XXX';
