<?php
defined('BASEPATH') or exit('No direct script access allowed');

$config['title']      = 'AdminLTE';
$config['title_mini'] = 'LT';
$config['title_lg']   = 'LTE';



/* Display panel login */
$config['auth_social_network'] = FALSE;
$config['forgot_password']     = TRUE;
$config['new_membership']      = TRUE;



/*
 * **********************
 * AdminLTE
 * **********************
 */
/* Page Title */
$config['pagetitle_open']     = '<h1>';
$config['pagetitle_close']    = '</h1>';
$config['pagetitle_el_open']  = '<small>';
$config['pagetitle_el_close'] = '</small>';

/* Breadcrumbs */
$config['breadcrumb_open']     = '<ol class="breadcrumb">';
$config['breadcrumb_close']    = '</ol>';
$config['breadcrumb_el_open']  = '<li>';
$config['breadcrumb_el_close'] = '</li>';
$config['breadcrumb_el_first'] = '<i class="fa fa-dashboard"></i>';
$config['breadcrumb_el_last']  = '<li class="active">';

$groups = [
  'admin' => 1,
  'admin_basic' => 2,
  'operator' => 3,
  'driver' => 4,
];

/* Menú Administración */
$config['menu_admin']  = [
  [ // ---> DASHBOARD 
    'title' => 'Dashboard',        
    'icon' => 'fa-dashboard',
    'route' => 'admin/dashboard',
    'permission' => [$groups['admin'], $groups['admin_basic'], $groups['operator'], $groups['driver']],
    'class' => 'dashboard',
    'function' => 'index'
  ],
  [ // ---> ADMINISTRACION

    'title' => 'Administración',     
    'icon' => 'fa-sun-o',
    'route' => '#',
    'permission' => [$groups['admin'], $groups['admin_basic']],
    'pages' => [						
      [
        'title' => 'Usuarios', // ---> USUARIOS        
        'icon' => 'fa-users',
        'route' => 'admin/users',
        'permission' => [$groups['admin'], $groups['admin_basic']],
        'class' => 'users',
        'function' => 'index'
      ],
      [
        'title' => 'Grupos', // ---> GRUPOS        
        'icon' => 'fa-th-list',
        'route' => 'admin/groups',
        'permission' => [$groups['admin'], $groups['admin_basic']],
        'class' => 'groups',
        'function' => 'index'
      ],
      [
        'title' => 'Preferencias', // ---> PREFERENCIAS        
        'icon' => 'fa-puzzle-piece',
        'route' => '#',
        'permission' => [$groups['admin'], $groups['admin_basic']],
        'pages' => [
          [
            'title' => 'Permisos', // ---> PERMISOS            
            'icon' => 'fa-braille',
            'route' => 'admin/prefs/permission',
            'permission' => [$groups['admin'], $groups['admin_basic']],
            'class' => 'prefs',
            'function' => 'permission'
          ],
          [
            'title' => 'Interfaces', // ---> INTERFACE ADMIN            
            'icon' => 'fa-window-maximize',
            'route' => 'admin/prefs/interfaces/admin',
            'permission' => [$groups['admin']],
            'class' => 'prefs',
            'function' => 'interfaces'
          ],
          [
            'title' => 'Base de Datos', // ---> BASE DE DATOS            
            'icon' => 'fa-database',
            'route' => 'admin/database',
            'permission' => [$groups['admin']],
            'class' => 'database',
            'function' => 'index'
          ],
        ],
      ],
      [
        'title' => 'Utilidades', // ---> UTILIDADES        
        'icon' => 'fa-plug',
        'route' => '#',
        'permission' => [$groups['admin']],
        'pages' => [
          [
            'title' => 'Archivo', // ---> ARCHIVO            
            'icon' => 'fa-file',
            'route' => 'admin/files',
            'permission' => [$groups['admin']],
            'class' => 'files',
            'function' => 'index'
          ],
          [
            'title' => 'Recursos', // ---> RECURSOS           
            'icon' => 'fa-plug',
            'route' => 'admin/resources',
            'permission' => [$groups['admin']],
            'class' => 'resources',
            'function' => 'index'
          ],
          [
            'title' => 'Licencia', // ---> LICENCIA            
            'icon' => 'fa-shopping-bag',
            'route' => 'admin/license',
            'permission' => [$groups['admin']],
            'class' => 'license',
            'function' => 'index'
          ],
        ],
      ],
    ]
  ],

  [ // ---> MODULOS
    'title' => 'Modulos',     
    'icon' => 'fa-modx',
    'route' => '#',
    'permission' => [$groups['admin'], $groups['admin_basic'], $groups['operator'], $groups['driver']],
    'pages' => [
      [
        'title' => 'Vehiculos', 
        'icon' => 'fa-newspaper-o',
        'route' => 'admin/vehiculos',
        'permission' => [$groups['admin'], $groups['admin_basic'], $groups['operator'], $groups['driver']],
        'class' => 'vehiculos',
        'function' => 'index'
      ],
    ],
  ],

];


/*
$config['menu_admin']  = [
  [ // ---> DASHBOARD 
    'title' => 'Dashboard',        
    'icon' => 'fa-dashboard',
    'route' => 'admin/dashboard',
    'permission' => [$groups['admin'], $groups['admin_basic'], $groups['operator'], $groups['driver']],
    'class' => ['dashboard'],
  ],
  [ // ---> ADMINISTRACION

    'title' => 'Administración',     
    'icon' => 'fa-sun-o',
    'route' => '#',
    'permission' => [$groups['admin'], $groups['admin_basic']],
    'class' => ['users', 'groups', 'prefs', 'database'],
    'pages' => [						
      [
        'title' => 'Usuarios', // ---> USUARIOS        
        'icon' => 'fa-users',
        'route' => 'admin/users',
        'permission' => [$groups['admin'], $groups['admin_basic']],
        'class' => 'users',
        'function' => 'index'
      ],
      [
        'title' => 'Grupos', // ---> GRUPOS        
        'icon' => 'fa-th-list',
        'route' => 'admin/groups',
        'permission' => [$groups['admin'], $groups['admin_basic']],
        'class' => 'groups',
        'function' => 'index'
      ],
      [
        'title' => 'Preferencias', // ---> PREFERENCIAS        
        'icon' => 'fa-puzzle-piece',
        'route' => '#',
        'permission' => [$groups['admin'], $groups['admin_basic']],
        'class' => ['prefs', 'database'],
        'pages' => [
          [
            'title' => 'Permisos', // ---> PERMISOS            
            'icon' => 'fa-braille',
            'route' => 'admin/prefs/permission',
            'permission' => [$groups['admin'], $groups['admin_basic']],
            'class' => 'prefs',
            'function' => 'permission'
          ],
          [
            'title' => 'Interfaces', // ---> INTERFACE ADMIN            
            'icon' => 'fa-window-maximize',
            'route' => 'admin/prefs/interfaces/admin',
            'permission' => [$groups['admin']],
            'class' => 'prefs',
            'function' => 'interfaces'
          ],
          [
            'title' => 'Base de Datos', // ---> BASE DE DATOS            
            'icon' => 'fa-database',
            'route' => 'admin/database',
            'permission' => [$groups['admin']],
            'class' => 'database',
            'function' => 'index'
          ],
        ],
      ],
      [
        'title' => 'Utilidades', // ---> UTILIDADES        
        'icon' => 'fa-plug',
        'route' => '#',
        'permission' => [$groups['admin']],
        'class' => ['files', 'resources', 'license'],
        'pages' => [
          [
            'title' => 'Archivo', // ---> ARCHIVO            
            'icon' => 'fa-file',
            'route' => 'admin/files',
            'permission' => [$groups['admin']],
            'class' => 'files',
            'function' => 'index'
          ],
          [
            'title' => 'Recursos', // ---> RECURSOS           
            'icon' => 'fa-plug',
            'route' => 'admin/resources',
            'permission' => [$groups['admin']],
            'class' => 'resources',
            'function' => 'index'
          ],
          [
            'title' => 'Licencia', // ---> LICENCIA            
            'icon' => 'fa-shopping-bag',
            'route' => 'admin/license',
            'permission' => [$groups['admin']],
            'class' => 'license',
            'function' => 'index'
          ],
        ],
      ],
    ]
  ],

  [ // ---> MODULOS
    'title' => 'Modulos',     
    'icon' => 'fa-modx',
    'route' => '#',
    'permission' => [$groups['admin'], $groups['admin_basic'], $groups['operator'], $groups['driver']],
    'class' => ['users', 'groups', 'prefs', 'database'],
    'pages' => [
      [
        'title' => 'Noticias', // ---> Noticias de Prueba!        
        'icon' => 'fa-newspaper-o',
        'route' => 'modules/noticias',
        'permission' => [$groups['admin'], $groups['admin_basic'], $groups['operator'], $groups['driver']],
        'class' => 'xxxx',
        'function' => 'xxxx'
      ],
    ],
  ],

];

*/



